import Icons from './Icons';
import Spinner from './Spinner';

export {
	Icons,
	Spinner
}