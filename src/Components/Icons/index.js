import React from 'react';
import { ReactSVG } from 'react-svg';
import right from './resources/right.svg';
import back from './resources/left-arrow.svg';
import bgPentagon from './resources/bg-pentagon.svg';
import bgTriangle from './resources/bg-triangle.svg';
import close from './resources/icon-close.svg';
import paper from './resources/icon-paper.svg';
import rock from './resources/icon-rock.svg';
import scissors from './resources/icon-scissors.svg';
import spock from './resources/icon-spock.svg';
import rules from './resources/image-rules.svg';
import rulesBonus from './resources/image-rules-bonus.svg';
import logo from './resources/logo.svg';
import logoBonus from './resources/logo-bonus.svg';
import lizard from './resources/icon-lizard.svg';

const RightIcon = () => <ReactSVG src={right} />;
const BackIcon = () => <ReactSVG src={back} />;
const BgPentagonIcon = () => <ReactSVG className="BgPentagonIcon" src={bgPentagon} />;
const BgTriangleIcon = () => <ReactSVG className="BgTriangleIcon" src={bgTriangle} />;
const CloseIcon = ({closeEvent}) => <ReactSVG onClick={closeEvent.bind(undefined)} src={close} />;
const LizardIcon = () => <ReactSVG src={lizard} />;
const PaperIcon = () => <ReactSVG src={paper} />;
const RockIcon = () => <ReactSVG src={rock} />;
const ScissorsIcon = () => <ReactSVG src={scissors} />;
const SpockIcon = () => <ReactSVG src={spock} />;
const RulesIcon = () => <ReactSVG src={rules} />;
const RulesBonusIcon = () => <ReactSVG src={rulesBonus} />;
const LogoIcon = () => <ReactSVG src={logo} />;
const LogoBonusIcon = () => <ReactSVG src={logoBonus} />;

export default {
	RightIcon,
	BackIcon,
	BgPentagonIcon,
	BgTriangleIcon,
	CloseIcon,
	LizardIcon,
	PaperIcon,
	RockIcon,
	ScissorsIcon,
	SpockIcon,
	RulesIcon,
	RulesBonusIcon,
	LogoIcon,
	LogoBonusIcon
}