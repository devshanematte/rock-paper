import React from 'react';
import Loader from 'react-loader-spinner';

const Spinner = ({
	color = "#2b3743",
	height = 80,
	width = 80,
	type = "BallTriangle",
}) => {

	return (
		<div className="spinner-block">
			<Loader type={type} color={color} height={height} width={width} />
		</div>
	)

}

export default Spinner