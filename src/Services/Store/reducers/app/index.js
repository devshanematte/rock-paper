const initialState = {
	version:'0.0.1',
	rules:false,
	selectType:'',
	selectTypePC:'',
	win:'',
	points:0
}

const app = (state = initialState, action: any) => {
	switch (action.type){
		case 'VIEW_RULES' :
			return {
				...state,
				rules:!state.rules
			}
		case 'SELECT_TYPE' :
			return {
				...state,
				selectType:action.select
			}
		case 'RESET_GAME' :
			return {
				...state,
				selectType:'',
				selectTypePC:'',
				win:''
			}
		case 'SELECT_TYPE_PC' :

			let win = '';
			//['rock', 'paper', 'scissors'];

			if(state.selectType == 'rock' && action.select == 'rock'){
				win = 'draw';

			}else if(state.selectType == 'rock' && action.select == 'paper'){
				win = 'lose';
				action.Sound.lose.play();
			}else if(state.selectType == 'rock' && action.select == 'scissors'){
				win = 'win';
				action.Sound.tada.play();
			}else if(state.selectType == 'paper' && action.select == 'paper'){
				win = 'draw';
			}else if(state.selectType == 'paper' && action.select == 'rock'){
				win = 'win';
				action.Sound.tada.play();
			}else if(state.selectType == 'paper' && action.select == 'scissors'){
				win = 'lose';
				action.Sound.lose.play();
			}else if(state.selectType == 'scissors' && action.select == 'scissors'){
				win = 'draw';
			}else if(state.selectType == 'scissors' && action.select == 'rock'){
				win = 'lose';
				action.Sound.lose.play();
			}else if(state.selectType == 'scissors' && action.select == 'paper'){
				win = 'win';
				action.Sound.tada.play();
			}

			return {
				...state,
				selectTypePC:action.select,
				win,
				points: win == 'win' ? Number(state.points) + 1 : win == 'lose' ? Number(state.points) - 1 : Number(state.points)
			}
		default :
			return state
	}
}

export default app