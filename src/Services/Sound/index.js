import {Howl, Howler} from 'howler';

const background = new Howl({
  src: ['/sounds/psychedelic.mp3'],
  autoplay: true,
  loop: true,
  volume: 0.05,
  onend: () => {
    console.log('Finished!');
  }
});

const clap = new Howl({
  src: ['/sounds/clap.mp3'],
  autoplay: false,
  volume: 0.25,
  onend: () => {
    console.log('Finished!');
  }
});

const fx1 = new Howl({
  src: ['/sounds/fx-1.mp3'],
  autoplay: false,
  volume: 0.25,
  onend: () => {
    console.log('Finished!');
  }
});

const fx2 = new Howl({
  src: ['/sounds/fx-2.mp3'],
  autoplay: false,
  volume: 0.25,
  onend: () => {
    console.log('Finished!');
  }
});

const fx3 = new Howl({
  src: ['/sounds/fx-3.mp3'],
  autoplay: false,
  volume: 0.25,
  onend: () => {
    console.log('Finished!');
  }
});

const tada = new Howl({
  src: ['/sounds/tada.mp3'],
  autoplay: false,
  volume: 0.02,
  onend: () => {
    console.log('Finished!');
  }
});

const lose = new Howl({
  src: ['/sounds/lose.mp3'],
  autoplay: false,
  volume: 0.1,
  onend: () => {
    console.log('Finished!');
  }
});

export default {
	background,
	clap,
  fx1,
  fx2,
  fx3,
  tada,
  lose
}