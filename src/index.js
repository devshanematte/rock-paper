import React from 'react';
import ReactDOM from 'react-dom';
import App from './Containers';
import * as serviceWorker from './serviceWorker';

import './Services/Styles/main.css';
import './Services/Styles/media/media.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
