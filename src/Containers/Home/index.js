import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Sound } from '../../Services';
import Score from './Helpers/score';
import Game from './Helpers/game';
import Rules from './Helpers/rules';

const Home = ({
	dispatch,
	rules,
	selectType,
	selectTypePC,
	win,
	points
}) => {

	useEffect(()=>{

	})

	return(
		<div className="main-block" unselectable="on" onClick={()=>{Sound.clap.play()}}>
			<div className="container">
				<div className="game-block">
					<Score points={points}/>
					<Game points={points} win={win} selectTypePC={selectTypePC} selectType={selectType} dispatch={dispatch}/>
					<Rules rules={rules} dispatch={dispatch}/>
				</div>
			</div>
		</div>
	)

}

const mapStateToProps = (state) => {
	return {
		rules: state.app.rules,
		selectType: state.app.selectType,
		selectTypePC: state.app.selectTypePC,
		win: state.app.win,
		points: state.app.points
	}
}

export default connect(mapStateToProps)(Home);