import React, { useEffect } from 'react';
import { Icons } from '../../../Components';

const Score = ({points}) => {

	useEffect(()=>{

	})

	return(
		<div className="score-block">
			<div className="score-block-title">
				<Icons.LogoIcon/>
			</div>
			<div className="score-block-points">
				<div className="score-block-points-content">
					<p>score</p>
					<h2>{ points }</h2>
				</div>
			</div>
		</div>
	)

}

export default Score;