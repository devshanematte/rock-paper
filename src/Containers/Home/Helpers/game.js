import React, { useEffect } from 'react';
import { Sound } from '../../../Services';
import { Icons } from '../../../Components';

const Game = ({
	dispatch,
	selectType,
	selectTypePC,
	win
}) => {

	useEffect(()=>{

	})

	const selectTypeEvent = (select) => {

		dispatch({type:'SELECT_TYPE',select:select})

		setTimeout(()=>{

			selectPC()

		}, 1500)

	}

	const selectPC = () => {

		let selectPcs = ['rock', 'paper', 'scissors'];
		let selectPc = selectPcs[Math.floor(Math.random()*selectPcs.length)];
		return dispatch({
			type:'SELECT_TYPE_PC',
			select:selectPc,
			Sound
		})

	}

	const resetGame = () => {
		return dispatch({type:'RESET_GAME'})
	}

	return(
		<div className="game-content">

			{
				selectType != '' ?
					
					<div className="game-content-selected" onLoad={()=>{alert()}}>
						<div className="game-content-selected-row">
							<div className="game-content-selected-left">
								<h2>you picked</h2>

								<div className={win == 'win' ? "you-picked-block winner" : "you-picked-block"}>

									<div className="circle circle-1"></div>
									<div className="circle circle-2"></div>
									<div className="circle circle-3"></div>

									{
										selectType == 'paper' ?
											<div className="button-player button-player-paper button-player-default" onMouseEnter={()=>{Sound.fx1.play()}}>
												<Icons.PaperIcon />
											</div>
										: selectType == 'scissors' ?
											<div className="button-player button-player-scissors button-player-default" onMouseEnter={()=>{Sound.fx2.play()}}>
												<Icons.ScissorsIcon />
											</div>
										: selectType == 'rock' ?
											<div className="button-player button-player-rock button-player-default" onMouseEnter={()=>{Sound.fx3.play()}}>
												<Icons.RockIcon />
											</div>
										: 
											<div onMouseEnter={()=>{Sound.fx2.play()}}></div>
									}
								</div>

							</div>
							<div className="game-content-selected-center">
								{
									win == 'draw' ?
										<h1>draw</h1>
									: win == 'win' ?
										<h1>you win</h1>
									: win == 'lose' ?
										<h1>you lose</h1>
									:
										<h1>Wait... :)</h1>
								}

								{
									win ?
										<span onClick={resetGame.bind(undefined)}>play again</span>
									:
										<div></div>
								}

							</div>
							<div className="game-content-selected-right">
								<h2>the house picked</h2>

								<div className={win == 'lose' ? "pc-picked-block winner" : "pc-picked-block"}>

									<div className="circle circle-1"></div>
									<div className="circle circle-2"></div>
									<div className="circle circle-3"></div>

									{
										selectTypePC == 'paper' ?
											<div className="button-player button-player-paper button-player-default" onMouseEnter={()=>{Sound.fx1.play()}}>
												<Icons.PaperIcon />
											</div>
										: selectTypePC == 'scissors' ?
											<div className="button-player button-player-scissors button-player-default" onMouseEnter={()=>{Sound.fx2.play()}}>
												<Icons.ScissorsIcon />
											</div>
										: selectTypePC == 'rock' ?
											<div className="button-player button-player-rock button-player-default" onMouseEnter={()=>{Sound.fx3.play()}}>
												<Icons.RockIcon />
											</div>
										: 
											<div className="button-player button-player-default button-player-default-pc" onMouseEnter={()=>{Sound.fx2.play()}}></div>
									}
								</div>
							</div>
						</div>

						<div className="game-content-selected-center game-content-selected-center-mobile">
							{
								win == 'draw' ?
									<h1>draw</h1>
								: win == 'win' ?
									<h1>you win</h1>
								: win == 'lose' ?
									<h1>you lose</h1>
								:
									<h1>Wait... :)</h1>
							}

							{
								win ?
									<span onClick={resetGame.bind(undefined)}>play again</span>
								:
									<div></div>
							}

						</div>

					</div>

				:
					<div className="game-content-1">
						<Icons.BgTriangleIcon />

						<div className="button-player button-player-paper" onClick={selectTypeEvent.bind(undefined, 'paper')} onMouseEnter={()=>{Sound.fx1.play()}}>
							<Icons.PaperIcon />
						</div>

						<div className="button-player button-player-scissors" onClick={selectTypeEvent.bind(undefined, 'scissors')} onMouseEnter={()=>{Sound.fx2.play()}}>
							<Icons.ScissorsIcon />
						</div>

						<div className="button-player button-player-rock" onClick={selectTypeEvent.bind(undefined, 'rock')} onMouseEnter={()=>{Sound.fx3.play()}}>
							<Icons.RockIcon />
						</div>
					</div>
			}

		</div>
	)

}

export default Game;