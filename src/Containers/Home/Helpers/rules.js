import React, { useEffect } from 'react';
import { Icons } from '../../../Components';

const Rules = ({
	dispatch,
	rules
}) => {

	useEffect(()=>{

	})

	return(
		<div>
			<div className="rules-block" onClick={()=>{dispatch({type:'VIEW_RULES'})}}>
				<h1>rules</h1>
			</div>
			<div className={!rules ? "rules-modal" : "rules-modal rules-modal-opened"}>
				<div className="rules-modal-content">
					<div className="rules-modal-content-header">
						<h1>Rules</h1>
						<Icons.CloseIcon closeEvent={()=>{dispatch({type:'VIEW_RULES'})}}/>
					</div>
					<div className="rules-modal-content-body">
						<Icons.RulesIcon />
					</div>
				</div>
			</div>
		</div>
	)

}

export default Rules;