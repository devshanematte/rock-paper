import React from 'react';
import { Provider } from 'react-redux';
import { Store } from '../Services';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Route, HashRouter, Router, Switch } from 'react-router-dom';

import Home from './Home';

const App = () => {

	const {
		store,
		persistor,
		history
	} = Store();

	return(
		<Provider store={store}>
			<PersistGate persistor={persistor}>
		      	<Router history={history}>
		      		<Switch>
				        <Route exact path={`/`} render={() => <Home history={history} /> } />
			        </Switch>
		    	</Router>
			</PersistGate>
		</Provider>
	)
}

export default App;